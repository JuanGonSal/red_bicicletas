const Bicicleta = require('../../models/bicicleta')


exports.bicicleta_list = function(req, res) {
  Bicicleta.allBicis().exec((err, bicis) => {
      if (err) {
          res.status(400).json({
              ok: false,
              err,
          });
      } else {
          res.status(200).json({
              bicicletas: bicis
          });
      }
  });
}

exports.bicicleta_create = function(req, res) {
  let bici = new Bicicleta({
                  code: req.body.code,
                  color: req.body.color,
                  modelo: req.body.modelo,
                  ubicacion: [req.body.lat, req.body.lng]
              });
  Bicicleta.add(bici, (err, newBici) => {
      res.status(200).json({
          bicicleta: newBici
      });
  });
}

exports.bicicleta_update = function(req, res) {
  let nuevaBici = {
      code: req.body.code,
      color: req.body.color,
      modelo: req.body.modelo,
      ubicacion: [req.body.lat, req.body.lng]
  };

  let bici = Bicicleta.findOneAndUpdate({code: req.body.id}, nuevaBici, () => {
      res.status(200).json({
          bicicleta: nuevaBici
      }); 
  });
}

exports.bicicleta_delete = function(req, res) {
  Bicicleta.removeByCode(req.body.code, (err, success) => {
      console.log("controllerAPI");
      console.log(req.body.code);
      res.status(204).send();
  });
}