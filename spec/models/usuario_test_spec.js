var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Reserva = require('../../models/reserva');
var Usuario = require('../../models/usuario');


describe('Testing Usuarios', function() {
    beforeEach(function(done) {
        mongoose.disconnect();

        let mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach(function(done) {
        Reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if (err) console.log(err);
                Bicicleta.deleteMany(function(err, success){
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

    afterAll((done) => { mongoose.connection.close(done) });

    describe('Cuando un Usuario reserva una bici', () => {
        it('debe existir la reserva', (done) => {
            const usuario = new Usuario({nombre: 'Cesar', email: "sdfs@gmail.com", password:"123456"});
            usuario.save();
            
            const bicicleta = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            bicicleta.save();

            let hoy = new Date();
            let maniana = new Date();
            maniana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, maniana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});