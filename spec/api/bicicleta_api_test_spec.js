var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = "http://localhost:3000/api/bicicletas";

describe('Bicicleta API', () => {
    beforeAll((done) => { mongoose.connection.close(done) });

    beforeEach((done) => {
        mongoose.disconnect();

        let mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', () => {
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, (error, response, body) => {
                let result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                
            });
            done();
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            let headers = {'content-type' : 'application/json'};
            let aBici = {"code": 10, "color": "verde", "modelo": "urbana", "lat": -33.243459, "lng": -58.028614};
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: JSON.stringify(aBici)
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                let bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe(aBici.color);
                expect(bici.modelo).toBe(aBici.modelo);
                expect(bici.ubicacion[0]).toBe(aBici.lat);
                expect(bici.ubicacion[1]).toBe(aBici.lng);
                
            });
            done();
        });
    });

    describe('POST BICICLETAS /delete', () => {
        it('Status 204', (done) => {
            let headers = {'content-type': 'application/json'};
            let a = new Bicicleta({code: 10, color: "rojo", modelo: "urbana", "lat": -33.243459, "lng": -58.028614});
            let aBiciId = { "id": a.code };
            Bicicleta.add(a);

            expect(Bicicleta.allBicis.length).toBe(1);
            
            request.delete({
                headers: headers,
                url: base_url + '/delete',
                body: JSON.stringify(aBiciId)
            }, (error, response, body) => {
                expect(response.statusCode).toBe(204);
                Bicicleta.allBicis((err, doc) => {
                    expect(doc.length).toBe(2);
                    
                });
                done();
            });
        });
    });

    describe('POST BICICLETAS /update', () => {
        it('Status 200', (done) => {
            let headers = {'content-type': 'application/json'};
            let a = new Bicicleta({code: 10, color: "rojo", modelo: "urbana", "lat": -33.243840, "lng":-58.029585});
            Bicicleta.add(a, () => {
                let headers = {'content-type': 'application/json'};
                let updateBici = {"code": a.code, "color": "verde", "modelo": "montaña", "lat": -33.245325, "lng": -58.035261};
                request.post({
                    headers: headers,
                    url: base_url + '/update',
                    body: JSON.stringify(updateBici)
                }, (error, response, body) => {
                    expect(response.statusCode).toBe(200);
                    let findBici = Bicicleta.findByCode(10, (err, doc) => {
                        expect(doc.code).toBe(10);
                        expect(doc.color).toBe(updateBici.color);
                        expect(doc.modelo).toBe(updateBici.modelo);
                        expect(doc.ubicacion[0]).toBe(updateBici.lat);
                        expect(doc.ubicacion[1]).toBe(updateBici.lng);
                    });
                });
                done();
            });
        });
    });
});
